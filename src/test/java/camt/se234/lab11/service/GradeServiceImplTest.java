package camt.se234.lab11.service;


import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.dao.StudentNewDaoImpl;
import camt.se234.lab11.entity.Student;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class GradeServiceImplTest{
    @Test
    public void testGetGrade(){
        GradeServiceImpl gradeService = new GradeServiceImpl();
        assertThat(gradeService.getGrade(100),is("A"));
        assertThat(gradeService.getGrade(80),is("A"));
        assertThat(gradeService.getGrade(78.9),is("B"));
        assertThat(gradeService.getGrade(75),is("B"));
        assertThat(gradeService.getGrade(74.4),is("C"));
        assertThat(gradeService.getGrade(60),is("C"));
        assertThat(gradeService.getGrade(59.4),is("D"));
        assertThat(gradeService.getGrade(33),is("D"));
        assertThat(gradeService.getGrade(32),is("F"));
        assertThat(gradeService.getGrade(0),is("F"));
    }
    @Test
    @Parameters(method = "paramsForTestGetGradeParams")
    @TestCaseName("Test getGrade Params [{Index}] : input is {0} , expect \"{1}\"")
    public void testGetGradeParams(double score,String expectedGrade){
        GradeServiceImpl gradeService=new GradeServiceImpl();
        assertThat(gradeService.getGrade(score),is(expectedGrade));
    }

    @Test
    public void testGetAverageGpa(){
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.getAverageGpa(),is(2.996));
    }
    @Test
    public void testAverageGpa() {
        StudentServiceImpl studentService = new StudentServiceImpl();
        StudentNewDaoImpl studentNewDao= new StudentNewDaoImpl();
        studentService.setStudentDao(studentNewDao);
        assertThat(studentService.getAverageGpa(),is(2.752));
    }

    @Test
    public void testFindByPartOfId() {
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123", "A", "temp", 2.33));
        mockStudents.add(new Student("124", "B", "temp", 2.33));
        mockStudents.add(new Student("223", "C", "temp", 2.33));
        mockStudents.add(new Student("224", "D", "temp", 2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentByPartOfId("22"), hasItem(new Student("223", "C", "temp", 2.33)));
        assertThat(studentService.findStudentByPartOfId("22"), hasItems(new Student("223", "C", "temp", 2.33), new Student("224", "D", "temp", 2.33)));

    }








    public Object paramsForTestGetGradeParams(){
        return new Object[][]{
                {100,"A"},
                {77,"B"}
        };
    }

public void textAllPossibleGrade(){
        GradeServiceImpl gradeService=new GradeServiceImpl();
        assertThat(gradeService.getGrade(100,100), is("A"));
        assertThat(gradeService.getGrade(75,75),is("B"));
        assertThat(gradeService.getGrade(70,70),is("C"));
        assertThat(gradeService.getGrade(50,50),is("D"));
        assertThat(gradeService.getGrade(10,10),is("F"));
}



}
