package camt.se234.lab11.service;

import camt.se234.lab11.dao.StudentDao;
import camt.se234.lab11.dao.StudentDaoImpl;
import camt.se234.lab11.dao.StudentNewDaoImpl;
import camt.se234.lab11.entity.Student;

import com.sun.corba.se.impl.orbutil.graph.NodeData;
import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.when;

public class StudentServiceImplTest {
    @Test
    public void testFindById() {
        StudentDaoImpl studentDao = new StudentDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        assertThat(studentService.findStudentById("123"), is(new Student("123", "A", "temp", 2.33)));
        assertThat(studentService.findStudentById("234"), is(new Student("234", "QWE", "c", 3.11)));
        assertThat(studentService.findStudentById("345"), is(new Student("345", "ERT", "HHH", 2.22)));
        assertThat(studentService.findStudentById("456"), is(new Student("456", "AA", "TTT", 3.33)));
        assertThat(studentService.findStudentById("567"), is(new Student("567", "ZXC", "GGG", 3.99)));
    }

    @Test

    public void testfindStudentById() {
        StudentNewDaoImpl studentNewDao = new StudentNewDaoImpl();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentNewDao);
        assertThat(studentService.findStudentById("123"), is(new Student("123", "A", "temp", 1.11)));
        assertThat(studentService.findStudentById("234"), is(new Student("234", "QWE", "c", 3.11)));
        assertThat(studentService.findStudentById("345"), is(new Student("345", "ERT", "HHH", 2.22)));
        assertThat(studentService.findStudentById("456"), is(new Student("456", "AA", "TTT", 3.33)));
        assertThat(studentService.findStudentById("567"), is(new Student("567", "ZXC", "GGG", 3.99)));
    }

    @Test
    public void testWithMock() {
        StudentDao studentDao = mock(StudentDao.class);
        List<Student> mockStudents = new ArrayList<>();
        StudentServiceImpl studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);
        mockStudents.add(new Student("123", "A", "temp", 2.33));
        mockStudents.add(new Student("124", "B", "temp", 2.33));
        mockStudents.add(new Student("223", "C", "temp", 2.33));
        mockStudents.add(new Student("224", "D", "temp", 2.33));
        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("123"), is(new Student("123", "A", "temp", 2.33)));
    }

    StudentDao studentDao;
    StudentServiceImpl studentService;

    @Before
    public void set() {
        studentDao = mock(StudentDao.class);
        studentService = new StudentServiceImpl();
        studentService.setStudentDao(studentDao);

    }

    @Test(expected = NoDataException.class)
    public void testNoDataException() {
        List<Student> mockStudents = new ArrayList<>();
        mockStudents.add(new Student("123", "A", "temp", 2.33));

        when(studentDao.findAll()).thenReturn(mockStudents);
        assertThat(studentService.findStudentById("55"), nullValue());
    }

    @Test(expected = ArithmeticException.class)
    public void testArithmeticException() {
       studentService.getAverageGpa();
    }


}

