package camt.se234.lab11.dao;

import camt.se234.lab11.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentNewDaoImpl implements StudentDao {
    List<Student> student;
    public StudentNewDaoImpl(){
        this.student = new ArrayList<>();
        this.student.add(new Student("123","A","temp",1.11));
        this.student.add(new Student("234","QWE","c",3.11));
        this.student.add(new Student("345","ERT","HHH",2.22));
        this.student.add(new Student("456","AA","TTT",3.33));
        this.student.add(new Student("567","ZXC","GGG",3.99));
    }

    @Override
    public List<Student> findAll() {
        return this.student;
    }
}
